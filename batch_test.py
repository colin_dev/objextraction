import sys, os, glob
from subprocess import call

def main():
    if (len(sys.argv) == 3):
        in_path_clean = os.path.normpath(sys.argv[1])
        if (os.path.exists(in_path_clean)):
            in_path_normal = os.path.join(os.path.dirname(in_path_clean), 'BackPure')
            if not (os.path.exists(in_path_normal)):
                print("'%s' doesn't exists" %(in_path_normal))
                return
        else:
            print("'%s' doesn't exists" %(sys.argv[1]))
            return
        out_path = os.path.normpath(sys.argv[2])
        if not (os.path.exists(out_path)):
            print("'%s' doesn't exists" %(sys.argv[2]))
            return
        print("Clean image path:  %s" %(in_path_clean))
        print("Normal image path: %s" %(in_path_normal))
        print("Output path:       %s" %(out_path))
        for file_name_full in glob.glob(in_path_clean + '/**/*.jpg', recursive=True):
            file_name_short = os.path.basename(file_name_full)
            (root, _) = os.path.splitext(file_name_short)
            file_name_parent_folder = os.path.basename(os.path.dirname(file_name_full))
            clean_img = file_name_full
            normal_img = os.path.join(in_path_normal, file_name_parent_folder, file_name_short)
            target_path = os.path.join(out_path, file_name_parent_folder)
            if not (os.path.exists(target_path)):
                os.mkdir(target_path)
            target_roi_clean = os.path.join(target_path, root + "_roi_clean.jpg")
            target_roi_normal = os.path.join(target_path, root + "_roi_normal.jpg")
            target_binarized = os.path.join(target_path, root + "_binarized.jpg")
            print("Process file %s" %(os.path.join(file_name_parent_folder, file_name_short)))
            #print(clean_img)
            #print(normal_img)
            #print(target_roi_clean)
            #print(target_roi_normal)
            #print(target_binarized)
            call([".\\bin\\x64\\Debug\\test.exe", clean_img, normal_img, target_roi_clean, target_roi_normal, target_binarized])
            if not (os.path.exists(target_roi_clean)):
                print("  Error")
            else:
                print("  OK")
    else:
        print('Parameter list: <in_path> <out_path>')

if __name__ == '__main__':
    main()
