// Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

extern "C" {
#include "ObjExt.h"
}

void show_help()
{
    printf("Usage:\n");
    printf("  1: test.exe in_pic out_pic\n");
    printf("     in_pic: input image file to detect\n");
    printf("     out_pic: output file\n");
    printf("  2: test.exe in_pic_1 in_pic_2 out_pic_1 out_pic_2\n");
    printf("     in_pic_1: input image file to used as clean image, no noise\n");
    printf("     in_pic_2: input image file to detect\n");
    printf("     out_pic_1: output image, roi from in_pic_1 which contains the target\n");
    printf("     out_pic_2: output image, roi from in_pic_2 which contains the target\n");
    printf("     out_pic_3: Optional. output image, binarized target\n");
}

int main(int argc, char** argv)
{
    if (argc == 3)
    {
        ObjExt(argv[1], NULL, argv[2], NULL, NULL);
    }
    else if (argc == 5)
    {
        ObjExt(argv[1], argv[2], argv[3], argv[4], NULL);
    }
    else if(argc == 6)
    {
        ObjExt(argv[1], argv[2], argv[3], argv[4], argv[5]);
    }
    else
    {
        show_help();
    }
    return 0;
}

