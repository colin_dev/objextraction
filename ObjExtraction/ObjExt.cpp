#include "stdafx.h"
#include "opencv2/opencv.hpp"
#include <Shlwapi.h>
#include <assert.h>
#include "ObjExt.h"

#define DEBUG_MODE 0
#define ELEMENT_SIZE 50
#define BORDER_THRESH 10
#define ALLOW_NON_SQUARE 1

extern "C" void ObjExt(char* pInPic1, char* pInPic2, char* pOutPic1, char* pOutPic2, char* pOutPic3)
{
    int err = 0;
    if (!pInPic1 || !PathFileExistsA(pInPic1))
    {
        std::cout << "input 1 doesn't exist!" << std::endl;
    }
    else if (!pOutPic1)
    {
        std::cout << "output file name is null!" << std::endl;
    }
    else
    {
        cv::Mat foreground = cv::imread(pInPic1);
        cv::Mat srcImage; 
        if (pInPic2 && PathFileExistsA(pInPic2))
        {
            srcImage = cv::imread(pInPic2);
        }
        else
        {
            srcImage = cv::imread(pInPic1);
        }

        if ((foreground.cols != srcImage.cols) || (foreground.rows != srcImage.rows))
        {
            if (DEBUG_MODE == 1)
            {
                printf("Mismatched size between clean image (%dx%d) and normal image (%dx%d), skip process.\n", foreground.cols, foreground.rows, srcImage.cols, srcImage.rows);
            }
            return;
        }

        if (DEBUG_MODE == 1)
        {
            printf("Process input image (%dx%d) ...\n", foreground.cols, foreground.rows);
        }

        // Convert to grayscale
        cv::Mat gray_image;
        cv::cvtColor(foreground, gray_image, CV_BGR2GRAY);

        // Adjust brightness and contract
        cv::Mat image_autolevel;
        int histSize = 256;
        double inputRange = 0, alpha = 0, beta = 0, minGray = 0, maxGray = 0;
        cv::minMaxLoc(gray_image, &minGray, &maxGray);
        inputRange = maxGray - minGray;
        alpha = (histSize - 1) / inputRange;
        beta = -minGray * alpha;
        foreground.convertTo(image_autolevel, -1, alpha, beta);

        // Re-convert to grayscalse
        cv::cvtColor(image_autolevel, gray_image, CV_BGR2GRAY);

        // Blur
        cv::Mat blur_image;
        cv::blur(gray_image, blur_image, cv::Size(ELEMENT_SIZE, ELEMENT_SIZE));

        // Get binary image using Canny edge detection
        cv::Mat canny_output;
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        int thresh = 10;
        Canny(blur_image, canny_output, thresh, thresh * 2, 3);
        findContours(canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        cv::Mat binary_image = cv::Mat::zeros(canny_output.size(), CV_8UC1);
        for (int i = 0; i< contours.size(); i++)
        {
            drawContours(binary_image, contours, i, cv::Scalar(255), 2, 8, hierarchy, 8);
        }
        cv::Mat de_noise = binary_image;
        cv::GaussianBlur(binary_image, de_noise, cv::Size(3, 3), 0, 0);

        // Dilate
        cv::Mat dilate_img;
        cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(ELEMENT_SIZE, ELEMENT_SIZE));
        //cv::dilate(de_noise, dilate_img, element);
        cv::dilate(binary_image, dilate_img, element);

        // Final contour detection
        findContours(dilate_img, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
        if ((contours.size() == 0) || hierarchy.size() == 0)
        {
            if (DEBUG_MODE == 1)
            {
                printf("  Can't detect any contour from input image, stop process.\n");
            }
            return;
        }

        // Find boundray box
        cv::Mat contoursImage(dilate_img.rows, dilate_img.cols, CV_8U, cv::Scalar(255));
        int index = 0;
        int max_width = 0;
        int max_height = 0;
        cv::Rect roi;
        for (; index >= 0; index = hierarchy[index][0]) {
            cv::Scalar color(rand() & 255, rand() & 255, rand() & 255);
            cv::drawContours(contoursImage, contours, index, cv::Scalar(0), 1, 8, hierarchy);
            cv::Rect rect = boundingRect(contours[index]);
            if (rect.width * rect.height > max_width * max_height) {
                roi = rect;
                max_width = rect.width;
                max_height = rect.height;
            }
            rectangle(contoursImage, rect, cv::Scalar(0, 0, 255), 3);
        }
        //cv::rectangle(contoursImage, roi, cv::Scalar(0, 0, 255), 3);

        if (DEBUG_MODE == 1)
        {
            printf("  roi found (%dx%d)@(%dx%d).\n", roi.width, roi.height, roi.x, roi.y);
        }

        cv::Rect newRoi = roi;
        bool squareForbid = false;
        bool skipImWrite = false;
        int dim = max(newRoi.width, newRoi.height);

        if (dim != newRoi.width) {
            int delta = dim - newRoi.width;
            if ((roi.x - delta / 2 > 0) && (roi.x + roi.width + delta / 2 < foreground.cols))
            {
                newRoi.x = roi.x - delta / 2;
                newRoi.width = dim;
            }
            else
            {
                newRoi = roi;
                squareForbid = true;
            }
            
        }
        if (dim != newRoi.height) {
            int delta = dim - newRoi.height;
            if ((roi.y - delta / 2 > 0) && (roi.y + roi.height + delta / 2 < foreground.rows))
            {
                newRoi.y = roi.y - delta / 2;
                newRoi.height = dim;
            }
            else
            {
                newRoi = roi;
                squareForbid = true;
            }
        }

        if (DEBUG_MODE == 1)
        {
            if (squareForbid)
            {
                printf("  roi too close to border, can't expand to square.\n");
            }
            else
            {
                printf("  expand roi to square (%dx%d)@(%dx%d).\n", newRoi.width, newRoi.height, newRoi.x, newRoi.y);
            }
        }

        bool expandBorder = false;
        if (!squareForbid && (BORDER_THRESH != 0))
        {
            if ((newRoi.x <= BORDER_THRESH) ||
                (newRoi.y <= BORDER_THRESH) ||
                (newRoi.x + newRoi.width + BORDER_THRESH >= foreground.cols) ||
                (newRoi.y + newRoi.height + BORDER_THRESH >= foreground.rows))
            {
                //skipImWrite = true;
            }
            else
            {
                expandBorder = true;
            }
        }

        if (expandBorder)
        {
            newRoi.x -= BORDER_THRESH;
            newRoi.y -= BORDER_THRESH;
            newRoi.width += BORDER_THRESH * 2;
            newRoi.height += BORDER_THRESH * 2;
            if (DEBUG_MODE == 1)
            {
                printf("  add border to roi (%dx%d)@(%dx%d).\n", newRoi.width, newRoi.height, newRoi.x, newRoi.y);
            }
        }
        else
        {
            if (DEBUG_MODE == 1)
            {
                printf("  will not add border to roi.\n");
            }
            if (!ALLOW_NON_SQUARE)
            {
                if (DEBUG_MODE == 1)
                {
                    printf("  does not allow non square output, stop process.\n");
                }
                return;
            }
        }


        cv::Mat outImageRoiClean, outImageRoi, outImageBinarized;
        if (!skipImWrite)
        {
            if (pOutPic1)
            {
                outImageRoiClean = foreground(newRoi);
                cv::imwrite(pOutPic1, outImageRoiClean);
            }

            if (pOutPic2)
            {
                outImageRoi = srcImage(newRoi);
                cv::imwrite(pOutPic2, outImageRoi);
            }

            if (pOutPic3)
            {
                cv::Mat roiImage = cv::Mat::ones(newRoi.size(), CV_8UC1);
                roiImage = roiImage.mul(gray_image(newRoi));
                cv::Mat roiBlur;
                cv::blur(roiImage, roiBlur, cv::Size(20, 20));

                cv::Mat roi_canny_output;
                std::vector<std::vector<cv::Point> > roi_contours;
                std::vector<cv::Vec4i> roi_hierarchy;
                int thresh = 10;
                Canny(roiBlur, roi_canny_output, thresh, thresh * 2, 3);
                findContours(roi_canny_output, roi_contours, roi_hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
                cv::Mat roiBinary = cv::Mat::zeros(roi_canny_output.size(), CV_8UC1);
                for (int i = 0; i< roi_contours.size(); i++)
                {
                    drawContours(roiBinary, roi_contours, i, cv::Scalar(255), CV_FILLED, 8, roi_hierarchy);
                }
                cv::Mat roiDeNoise = roiBinary;
                cv::GaussianBlur(roiBinary, roiDeNoise, cv::Size(3, 3), 0, 0);

                cv::Mat roiDilate;
                cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(10, 10));
                cv::dilate(roiDeNoise, roiDilate, element);

                threshold(roiDilate, roiDilate, 50, 200.0, CV_THRESH_BINARY);
                roiImage = roiDilate.mul(cv::Mat::ones(roiDilate.size(), CV_8UC1) * 255);

                cv::Size m_Size = roiImage.size();
                cv::Mat temimage = cv::Mat::zeros(m_Size.height + 2, m_Size.width + 2, roiImage.type());
                roiImage.copyTo(temimage(cv::Range(1, m_Size.height + 1), cv::Range(1, m_Size.width + 1)));
                floodFill(temimage, cv::Point(0, 0), cv::Scalar(255));
                cv::Mat cutImg;
                temimage(cv::Range(1, m_Size.height + 1), cv::Range(1, m_Size.width + 1)).copyTo(cutImg);
                cv::Mat dstimage = roiImage | (~cutImg);

                outImageBinarized = cv::Mat::zeros(srcImage.size(), CV_8UC1);
                dstimage.copyTo(outImageBinarized(newRoi));

                cv::cvtColor(outImageBinarized, outImageBinarized, cv::COLOR_GRAY2BGR);

                cv::imwrite(pOutPic3, outImageBinarized);
            }
        }

        if (DEBUG_MODE == 1)
        {
            cv::namedWindow("Source-Gray", CV_WINDOW_NORMAL);
            cv::imshow("Source-Gray", gray_image);

            cv::namedWindow("Source-Gray-Blur", CV_WINDOW_NORMAL);
            cv::imshow("Source-Gray-Blur", blur_image);

            cv::namedWindow("Binary", CV_WINDOW_NORMAL);
            cv::imshow("Binary", binary_image);
            
            cv::namedWindow("De-noise", CV_WINDOW_NORMAL);
            cv::imshow("De-noise", de_noise);

            cv::namedWindow("Dilate", CV_WINDOW_NORMAL);
            cv::imshow("Dilate", dilate_img);

            cv::namedWindow("Contour", CV_WINDOW_NORMAL);
            cv::imshow("Contour", contoursImage);

            cv::namedWindow("OutputROI", CV_WINDOW_NORMAL);
            cv::imshow("OutputROI", outImageRoi);

            if (pOutPic2)
            {
                cv::namedWindow("OutputROIClean", CV_WINDOW_NORMAL);
                cv::imshow("OutputROIClean", outImageRoiClean);
            }

            if (!pOutPic3)
            {
                cv::namedWindow("OutputBinarized", CV_WINDOW_NORMAL);
                cv::imshow("OutputBinarized", outImageBinarized);
            }

            cv::waitKey(0);
        }

        outImageRoi.release();
        outImageBinarized.release();
        contoursImage.release();
        element.release();
        dilate_img.release();
        de_noise.release();
        binary_image.release();
        canny_output.release();
        image_autolevel.release();
        gray_image.release();
        srcImage.release();
        foreground.release();
    }
}
